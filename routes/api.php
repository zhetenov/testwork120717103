<?php

use App\Http\Controllers\V1\CategoryController;
use Illuminate\Support\Facades\Route;

Route::prefix('v1')->group(function () {
    Route::get('categories', [CategoryController::class, 'getCategories']);
    Route::post('categories', [CategoryController::class, 'addCategory']);
    Route::put('categories/{id}', [CategoryController::class, 'editCategory']);
    Route::delete('categories/{id}', [CategoryController::class, 'deleteCategory']);
});