<?php

declare(strict_types=1);

namespace App\Services\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Trait RestExceptionHandler
 * @package App\Services\Traits
 */
trait RestExceptionHandler
{
    /**
     * @param string $message
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    private function resp(string $message = '', int $statusCode = 200):JsonResponse
    {

        return response()->json([
            'message' => $message,
        ], 400);
    }

    /**
     * Determines if request is an api call.
     *
     * If the request URI contains '/api'.
     *
     * @param Request $request
     * @return bool
     */
    protected function isApiCall(Request $request): bool
    {
        return strpos($request->getUri(), '/api') !== false;
    }

    /**
     * Creates a new JSON response based on exception type.
     *
     * @param Request $request
     * @param \Throwable $e
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getJsonResponseForException(Request $request, \Throwable $e): JsonResponse
    {
        return $this->resp($e->getMessage(), 400);
    }
}
