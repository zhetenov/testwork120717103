<?php

declare(strict_types=1);

namespace App\Services\Handlers;

use App\Models\Category;
use App\Services\DTO\CategoryDTO;

/**
 * Class AddCategoryHandler.
 */
final class AddCategoryHandler
{
    /**
     * @param CategoryDTO $categoryDTO
     * @return Category
     */
    public function handle(CategoryDTO $categoryDTO): Category
    {
        return Category::create($categoryDTO->toArray());
    }
}
