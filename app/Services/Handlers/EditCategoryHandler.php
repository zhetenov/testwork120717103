<?php

declare(strict_types=1);

namespace App\Services\Handlers;

use App\Models\Category;
use App\Services\DTO\CategoryDTO;

/**
 * class EditCategoryHandler.
 */
final class EditCategoryHandler
{
    /**
     * @param Category $category
     * @param CategoryDTO $categoryDTO
     */
    public function handle(Category $category, CategoryDTO $categoryDTO): void
    {
        $category->update($categoryDTO->toArray());
    }
}
