<?php

declare(strict_types=1);

namespace App\Services\Handlers;

use App\Models\Category;

/**
 * Class GetCategoriesHandler.
 */
final class GetCategoriesHandler
{
    /**
     * @return Category[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function handle()
    {
        return Category::query()
            ->orderBy('title')
            ->get();
    }
}
