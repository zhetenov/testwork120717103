<?php

declare(strict_types=1);

namespace App\Services\DTO;

/**
 * Class CategoryDTO.
 */
final class CategoryDTO
{
    private string $title;

    private string $description;

    /**
     * @param array $data
     * @return static
     */
    public static function fromArray(array $data): self
    {
        $self = new CategoryDTO();
        $self->description = $data['description'];
        $self->title = $data['title'];

        return $self;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'title'         => $this->title,
            'description'   => $this->description,
        ];
    }
}
