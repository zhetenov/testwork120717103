<?php

namespace App\Exceptions;

use App\Services\Traits\RestExceptionHandler;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class Handler extends ExceptionHandler
{
    use RestExceptionHandler;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  Request  $request
     * @param  \Throwable $e
     * @return Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $e): Response
    {
        if(!$this->isApiCall($request)) {
            $result = parent::render($request, $e);
        } else {
            $result = $this->getJsonResponseForException($request, $e);
        }
        return $result;
    }
}
