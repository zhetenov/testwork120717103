<?php

declare(strict_types=1);

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\EditCategoryRequest;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use App\Services\Handlers\AddCategoryHandler;
use App\Services\Handlers\EditCategoryHandler;
use App\Services\Handlers\GetCategoriesHandler;
use Illuminate\Http\JsonResponse;

/**
 * Class CategoryController.
 */
final class CategoryController extends Controller
{
    /**
     * @param GetCategoriesHandler $handler
     * @return JsonResponse
     */
    public function getCategories(GetCategoriesHandler $handler): JsonResponse
    {
        $data = $handler->handle();
        return response()->json(CategoryResource::collection($data));
    }

    /**
     * @param Category $category
     * @param EditCategoryRequest $request
     * @param EditCategoryHandler $handler
     * @return JsonResponse
     */
    public function editCategory(Category $category, EditCategoryRequest $request, EditCategoryHandler $handler): JsonResponse
    {
        $handler->handle($category, $request->getDTO());
        return response()->json([]);
    }

    /**
     * @param CreateCategoryRequest $request
     * @param AddCategoryHandler $handler
     * @return JsonResponse
     */
    public function addCategory(CreateCategoryRequest $request, AddCategoryHandler $handler): JsonResponse
    {
        $category = $handler->handle($request->getDTO());
        return response()->json(new CategoryResource($category), 201);
    }

    /**
     * @param Category $category
     * @return JsonResponse
     */
    public function deleteCategory(Category $category): JsonResponse
    {
        $category->delete();
        return response()->json([]);
    }
}
