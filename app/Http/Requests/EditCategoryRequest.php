<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Services\DTO\CategoryDTO;
use Illuminate\Foundation\Http\FormRequest;

/**
 * EditCategoryRequest.
 */
final class EditCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'required',
            'description' => 'required',
        ];
    }

    /**
     * @return CategoryDTO
     */
    public function getDTO(): CategoryDTO
    {
        return CategoryDTO::fromArray($this->toArray());
    }
}
