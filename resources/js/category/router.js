import router from '../router'
import CategoryList from './pages/CategoryList';
import AddCategory from './pages/AddCategory';
import EditCategory from './pages/EditCategory';


router.addRoutes([
    {
        path: '/categories',
        name: 'categories',
        component: CategoryList,
    },
    {
        path: '/categories/add',
        name: 'categories-add',
        component: AddCategory,
    },
    {
        path: '/categories/edit/:id',
        name: 'categories-edit',
        component: EditCategory,
        props: true,
    },
])