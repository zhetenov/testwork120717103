import store from './../store'
import axios from "axios";

const state = {
    categories: []
}

const getters = {
    getCategories(state) {
        return state.categories
    }
}

const mutations = {
    SET_CATEGORIES(state, categories) {
        state.categories = categories
    }
}

const actions = {
    fetchCategories({commit}) {
        return axios.get('http://127.0.0.1:8000/api/v1/categories')
            .then((response) => {
                commit('SET_CATEGORIES', response.data)
                console.log(response.data);
            })
            .catch((error) => {
               console.log('error', error);
            })
    },
    addCategory({commit}, payload) {
        return axios.post('http://127.0.0.1:8000/api/v1/categories', payload)
            .then((response) => {
                console.log(response.data);
            })
            .catch((error) => {
                console.log('error', error);
            })
    },
    deleteCategory({commit}, payload) {
        return axios.delete('http://127.0.0.1:8000/api/v1/categories/' + payload)
            .then((response) => {
                console.log(response.data);
            })
            .catch((error) => {
                console.log('error', error);
            })
    },
    editCategory({commit}, payload) {
        return axios.put('http://127.0.0.1:8000/api/v1/categories/'+payload.id, payload)
            .then((response) => {
                console.log(response.data);
            })
            .catch((error) => {
                console.log('error', error);
            })
    }
}

store.registerModule('category', {
    state, getters, mutations, actions
})